variable dc {
    description = "Name/IP of the domain controller to work against"
    type = string
    default = "lab-dc1.ad.lab.lostroncos.net"
}

variable dc_username {
    description = "Username to use when connecting to DC"
    type = string
    default = "administrator"
}

variable dc_password {
    description = "password to use when connecting to DC"
    type = string
    default = "VMw@re!23z"
}
variable base_dn {
    description = "base DN"
    type = string
    default = "DC=example,dc=com"
}
variable default_password {
    description = "default password to assign to new accounts"
    type = string
    default = "VMw@re!23z"
}

variable default_admin_password{
    description = "default password to assign to new accounts"
    type = string    
    default = "VMw@re!23z"    
}

variable  email_domain {
    type = string  
    description = "Email Domain to associate with user accounts"
    default = "example.com"
}
variable user_list {
    type = map(object({
        sam_account_name = string
        display_name = string
        container = string
        description = string
        email_address =string
        given_name = string
        surname =string
    }))

}

variable ou_list {
    type = map (object({
        name = string
        rel_path = string
        description = string
        protected = bool 

    }))
}

variable new_group_list{
    type = map(object({
        name=string
        sam_account_name = string
        scope = string
        category = string
        container =string
        group_members = list(string)

    }))
}

variable builtin_group_list{
    type = map(object({
        name=string
        sam_account_name = string
        group_members = list(string)

    }))
}

