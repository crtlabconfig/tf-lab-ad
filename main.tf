terraform {
  required_providers {
    ad = {
      source  = "hashicorp/ad"
      version = "0.4.1"
    }
  }
  backend "http" {
    skip_cert_verification = true
    #    address = "https://gitlab.lab.lostroncos.net/api/v4/projects/17/terraform/state/ad-config"
    #    lock_address = "https://gitlab.lab.lostroncos.net/api/v4/projects/17/terraform/state/ad-config/lock"
    #    unlock_address = "https://gitlab.lab.lostroncos.net/api/v4/projects/17/terraform/state/ad-config/lock"
    #    lock_method="POST"
    #    unlock_method="DELETE"
    #    retry_wait_min=5

  }
}

/*
 terraform init  -backend-config="username=<username>" -backend-config="password=<gitlabtoken>"


 */

// remote using Basic authentication
provider "ad" {
  winrm_hostname = var.dc
  winrm_username = var.dc_username
  winrm_password = var.dc_password
  winrm_use_ntlm = true
}

locals {
  default_users_dn = "CN=users,${var.base_dn}"
}


resource "ad_ou" "new_ous" {
  for_each    = var.ou_list
  name        = each.value.name
  path        = each.value.rel_path != "" ? "${each.value.rel_path},${var.base_dn}" : var.base_dn
  description = each.value.description
  protected   = each.value.protected
}

#locals {
#    dns ={
#        for  k,v in ad_ou.new_ous:
#        k => v.dn
#
#    }
#}

#
#data "ad_ou" "created_ous"{
#    depends_on = [ ad_ou.new_ous ]
#    for_each = ad_ou.new_ous
#    dn = each.value.dn
#}


resource "ad_user" "users" {
  for_each               = var.user_list
  sam_account_name       = each.value.sam_account_name
  principal_name         = each.value.sam_account_name
  display_name           = each.value.display_name
  given_name             = each.value.given_name
  surname                = each.value.surname
  description            = each.value.description
  container              = each.value.container != "" ? ad_ou.new_ous[each.value.container].dn : local.default_users_dn
  password_never_expires = true
  cannot_change_password = false
  email_address          = "${each.value.email_address}@${var.email_domain}"
  initial_password       = var.default_password

  # these also work
  #container = "${each.value.container != ""? local.dns[(each.value.container)]: var.base_dn}"        
  #container = lookup(local.dns,(each.value.container),local.default_users_dn)

}

data "ad_user" "u" {
  user_id = "svcsalt"
}

output "username" {
  value = data.ad_user.u.sam_account_name
}

#output "country" {
#    value = data.ad_user.u.country
#}

output "trusted_for_delegation" {
  value = data.ad_user.u.trusted_for_delegation
}

output "all" {
  value = data.ad_user.u
}
