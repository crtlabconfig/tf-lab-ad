# dc =  from cli
# dc_user = from cli
# dc_password = from cli 
#default password from cli
# default admin password = from cli


user_list = {
  "jdoe" = {
    "sam_account_name" = "jdoe"
    "display_name"     = "Jane Doe"
    "given_name"       = "Jane"
    "surname"          = "Doe"
    "container"        = "vmwareusers"
    "description"      = "A User Account"
    "email_address"    = "jdoe"
  },
  "jdelaney" = {
    "sam_account_name" = "jdelaney"
    "display_name"     = "Jame Delaney"
    "given_name"       = "James"
    "surname"          = "Delaney"
    "container"        = "vmwareusers"
    "description"      = "A User Account"
    "email_address"    = "jdoe"
  },
  "pneruda" = {
    "sam_account_name" = "pneruda"
    "display_name"     = "Pablo Neruda"
    "given_name"       = "Pablo"
    "surname"          = "Neruda"
    "container"        = "vmwareusers"
    "description"      = "A User Account"
    "email_address"    = "pneruda"
  },
  "opaz" = {
    "sam_account_name" = "opaz"
    "display_name"     = "Octavio Paz"
    "given_name"       = "Octavio"
    "surname"          = "Paz"
    "container"        = "vmwareusers"
    "description"      = "A User Account"
    "email_address"    = "opaz"
  },
  "carsadmin" = {
    "sam_account_name" = "cars"
    "display_name"     = "Carlos Tronco - Admin"
    "given_name"       = "Carlos"
    "surname"          = "Tronco-Admin"
    "container"        = "vmwareusers"
    "description"      = ""
    "email_address"    = "cars"
  },
  "carsuser" = {
    "sam_account_name" = "ctronco"
    "display_name"     = "Carlos Tronco"
    "given_name"       = "Carlos"
    "surname"          = "Tronco"
    "container"        = "vmwareusers"
    "description"      = "My regular User Account"
    "email_address"    = "ctronco"
  },
  "vraldap" = {
    "sam_account_name" = "svcvraldap"
    "display_name"     = "Service VRA LDAP"
    "given_name"       = "Service"
    "surname"          = "VRALDAP"
    "container"        = "vmwareusers"
    "description"      = "LDAP Binding account for vRA"
    "email_address"    = "svcvraldap"
  },
  "vcenterldap" = {
    "sam_account_name" = "svcvcldap"
    "display_name"     = "Service VCSA LDAP"
    "given_name"       = "Service"
    "description"      = "LDAP binding account for vCenter"
    "surname"          = "VCLDAP"
    "container"        = "vmwareusers"
    "email_address"    = "svcvcldap"
  },
  "vraiaas" = {
    "sam_account_name" = "svcvra"
    "display_name"     = "Service VRA IaaS USer"
    "description"      = ""
    "given_name"       = "Service"
    "surname"          = "VRA"
    "container"        = "vmwareusers"
    "email_address"    = "svcvra"
  },
  "vrlcm" = {
    "sam_account_name" = "svcvrlcm"
    "description"      = "LifeCycleManager account for vCenter access"
    "display_name"     = "vRLifecycleManager"
    "given_name"       = "Service"
    "surname"          = "VRlcm"
    "container"        = "vmwareusers"
    "email_address"    = "svcvrlcm"
  },
  "vrajenkins" = {
    "sam_account_name" = "vraJenkins"
    "description"      = "Account used by vRA to access Jenkins"
    "display_name"     = "vRA Jenkins Test Acct"
    "given_name"       = "vRA"
    "surname"          = "Jenkins"
    "container"        = "vmwareusers"
    "email_address"    = "vrajenkins"
  },
  "jenkinsvcenter" = {
    "sam_account_name" = "svcjenkinsvc"
    "description"      = "Account for Jenkins to Access vCenter"
    "display_name"     = "Jenkins vCentet"
    "given_name"       = "vcenter"
    "surname"          = "Jenkins"
    "container"        = "vmwareusers"
    "email_address"    = "svcjenkinsvc"
  },
  "saltldap" = {
    "sam_account_name" = "svcsaltldap"
    "description"      = "LDAP binding account for Salt"
    "display_name"     = "Salt LDAP connection"
    "given_name"       = "Salt"
    "surname"          = "LDAP"
    "container"        = "vmwareusers"
    "email_address"    = "svcsaltldap"
  },
  "saltservice" = {
    "sam_account_name" = "svcsalt"
    "description"      = "Generic Salt service account"
    "display_name"     = "Salt Service Account"
    "given_name"       = "Salt"
    "surname"          = "Service"
    "container"        = "vmware"
    "email_address"    = "svcsalt"
  },
  "zabbix" = {
    "sam_account_name" = "svczabbix"
    "description"      = ""
    "display_name"     = "Zabbix Service Account"
    "given_name"       = "Service"
    "surname"          = "Zabbix"
    "container"        = ""
    "email_address"    = "svczabbix"
  },
  "packer" = {
    "sam_account_name" = "svcpacker"
    "description"      = ""
    "display_name"     = "Zabbix Service Account"
    "given_name"       = "Service"
    "surname"          = "Packer"
    "container"        = ""
    "email_address"    = "svcpacker"
  },
  "vra7sql" = {
    "sam_account_name" = "svcvra7sql"
    "description"      = "SQL Server Service Account"
    "display_name"     = "Vra7 SQLSvc Account"
    "given_name"       = "Service"
    "surname"          = "vra7sql"
    "container"        = "vmwareusers"
    "email_address"    = "svcvra7sql"
  },
  "vra7iaas" = {
    "sam_account_name" = "svcvra7iaas"
    "description"      = "vRA7 Iaas Service Account"
    "display_name"     = "Vra7 IaaSSvc Account"
    "given_name"       = "Service"
    "surname"          = "vra7IaaS"
    "container"        = "vmwareusers"
    "email_address"    = "svcvra7iaas"
  },
  "vra7web" = {
    "sam_account_name" = "svcvra7web"
    "description"      = "vRA7 web Service Account"
    "display_name"     = "Vra7 WebSvc Account"
    "given_name"       = "Service"
    "surname"          = "vra7Web"
    "container"        = "vmwareusers"
    "email_address"    = "svcvra7web"
  },
  "vra7dem" = {
    "sam_account_name" = "svcvra7dem"
    "description"      = "vRA7 Dem Service Account"
    "display_name"     = "Vra7 DemSvc Account"
    "given_name"       = "Service"
    "surname"          = "vra7Dem"
    "container"        = "vmwareusers"
    "email_address"    = "svcvra7Dem"
  },
  "vra7ldap" = {
    "sam_account_name" = "svcvra7ldap"
    "description"      = "vRA7 LDAP Service Account"
    "display_name"     = "Vra7 LDAPSvc Account"
    "given_name"       = "Service"
    "surname"          = "vra7Ldap"
    "container"        = "vmwareusers"
    "email_address"    = "svcvra7ldap"
  },
  "vra7vcenter" = {
    "sam_account_name" = "svcvra7vc"
    "description"      = "vRA7 Vcenter Service Account"
    "display_name"     = "Vra7 VCSvc Account"
    "given_name"       = "Service"
    "surname"          = "vra7VC"
    "container"        = "vmwareusers"
    "email_address"    = "svcvra7vc"
  }

}

ou_list = {
  "vmware" = {
    "name"        = "VMware"
    "rel_path"    = ""
    "description" = "VMware Master  OU"
    protected     = true
  },
  "vmwareusers" = {
    name          = "Users"
    rel_path      = "OU=VMware"
    "description" = "VMware Specific User Accounts"
    protected     = true
  },
  "vmwaregroups" = {
    "name"        = "Groups"
    "rel_path"    = "OU=VMware"
    "description" = "VMware Specific Groups"
    protected     = true
  }
  "vracomputers" = {
    "name"        = "vRAServers"
    "rel_path"    = "OU=VMware"
    "description" = "VRA Created Computer Accounts"
    protected     = true
  }
}

new_group_list = {
  vmwareadmins = {
    "name"             = "VMWareAdmins"
    "sam_account_name" = "VMWareAdmins"
    "scope"            = "global"
    "category"         = "security"
    "description"      = "VMWare admin accounts"
    "container"        = "f"
    group_members      = ["cars", "svcvraldap"]
  }
}
builtin_group_list = {
  domainadmins = {
    "name"             = "Domain Admins"
    "sam_account_name" = ""
    group_members      = ["cars", "svcvraldap"]
  },
  Administrators = {
    "name"             = "Administrators"
    "sam_account_name" = ""
    group_members      = ["cars", "administrator", "enterprise admins", "domain admins"]
  }
}


