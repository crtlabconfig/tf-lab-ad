# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/ad" {
  version     = "0.4.1"
  constraints = "0.4.1"
  hashes = [
    "h1:8FGiw3oAjJkxdMulKYUGwZH+NiO4ugTMxAGDd/ArikQ=",
    "zh:05c722e1ddeaee2c1ab13760b044977b89561960053666e186825bc94e0c5a71",
    "zh:14961f21ffe8c0524aa21020696a60c9ef57e1d23a2dd161ec8b0a410cbca200",
    "zh:1aebdcf75ea2458195713a762b6748a1f590c4d2b1b1f7906fab3f4b627ed1f2",
    "zh:24ccbdf40d4b6d66c7954e3fa1334440cc1d2b2b2cf683673cb648d2bb8ca440",
    "zh:25ecfc97ffedc8b357c6e5c228dbccf96cc1cda05b809b5f71ca8c02ebd047fc",
    "zh:37b3ee26d16305a2ebd668d6b3fe0743ad3abb7024c0951202a7cb07ea0b9d8c",
    "zh:70674c6fd4badcac1a0df60f9f6607ee0ddc18983bd1ca2b5aa75c46a21ae0ae",
    "zh:9ce8b6ee4d8b3f7d821e0c18817c5eae2420f5d3a65216fecd292cdc28d89e2b",
    "zh:f5e5f01987f6f0f2c93b5cef90e14a45fb303418e55c88313d99171d71c341cd",
    "zh:f6f84f7467838e7e858a1f5f0df58edb90cfa5ac9d6c20a76d16502ebc28e457",
  ]
}
